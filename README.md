[![Install directly with Stylus][badge]][style]

Dark-Switcher is a global-settings companion-userstyle for my [userstyles][].

[userstyles]: https://gitlab.com/vednoc/userstyles
[badge]: https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59.svg?longCache=true&style=for-the-badge
[style]: https://gitlab.com/vednoc/dark-switcher/raw/master/switcher.user.styl

## Features
* Supports all color variables for my userstyles.
* Supports common options you might want to override.
* Supports DeepDark userstyles made by [RaitaroH][].
* Configurable, very extendable, and easy to use.

[RaitaroH]: https://gitlab.com/RaitaroH

## Contributing
Any contribution is greatly appreciated. Consider supporting me on [ko-fi][] or
on [LiberaPay][] if you find my projects useful.

[ko-fi]: https://ko-fi.com/vednoc
[LiberaPay]: https://liberapay.com/vednoc

## Contributors
- [vednoc](https://gitlab.com/vednoc) - creator, maintainer
